import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormlyFieldConfig, FormlyModule } from '@ngx-formly/core';
import { AbstractControl, FormsModule, ReactiveFormsModule, ValidationErrors } from '@angular/forms';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { CommonModule } from '@angular/common';
import { FormlyFieldButton } from './button-type.component';
import { PanelWrapperComponent } from './panel-wrapper.component';
import { RepeatDeleteTypeComponent } from './repeat-delete.type';
import { RepeatTypeComponent } from './repeat-sectio.type';
// import { FormlyPrimeNGModule } from '@ngx-formly/primeng';

// declaration n imports
@NgModule({
  declarations: [
    AppComponent, FormlyFieldButton, PanelWrapperComponent, RepeatDeleteTypeComponent, RepeatTypeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormlyModule.forRoot(
      {
        validationMessages: [
          { name: 'required', message: 'This field is required' },
          { name: 'minLength', message: minLengthValidationMessage },
          { name: 'email', message: emailValidatorMessage },
          { name: 'fieldMatch', message: passwordValidatorMessage },
          { name: 'dob', message: 'User cannot be less than 18 years old' }
        ],
        validators: [
          { name: 'password', validation: passwordValidator },
          { name: 'fieldMatch', validation: fieldMatchValidator }
        ],
        types: [
          {
            name: 'button',
            component: FormlyFieldButton,
            wrappers: ['form-field'],
            defaultOptions: {
              props: {
                btnType: 'default',
                type: 'button',
              },
            },
          },
          {
            name: 'repeat-delete',
            component: RepeatDeleteTypeComponent,
            defaultOptions: {
              templateOptions: {
                btnType: 'default',
                type: 'button',
              },
            },
          },
          { name: 'repeat', component: RepeatTypeComponent },

        ],
        wrappers: [
          { name: 'panel', component: PanelWrapperComponent },
        ],

      },

    ),
    ReactiveFormsModule,
    FormlyBootstrapModule,
    FormsModule,
    CommonModule
    // FormlyPrimeNGModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

// Minimum length validator
export function minLengthValidationMessage(error: any, field: FormlyFieldConfig) {
  return `${field.props?.label} Should be at least of ${field.props?.minLength} characters`;
}

// Email validator
export function emailValidator(control: AbstractControl): ValidationErrors | null {
  return !control.value || /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/.test(control.value) ? null : { email: true };
}
export function emailValidatorMessage(error: any, field: FormlyFieldConfig) {
  return `"${field.formControl?.value}" is not a valid Email Address`;
}
const hasUppercase = /[A-Z]/;
const hasNumber = /\d/;
const hasLowerCase = /[a-z]/;

// Password uppercase and number validator
export function passwordValidator(control: AbstractControl): ValidationErrors | null {
  const hasUppercase = /[A-Z]/.test(control.value);
  const hasNumber = /\d/.test(control.value);

  if (!hasUppercase && !hasNumber) {
    return { uppercaseNumberMissing: true };
  } else if (!hasUppercase) {
    return { uppercaseMissing: true };
  } else if (!hasNumber) {
    return { numberMissing: true };
  }

  return null;
}

export function passwordValidatorMessage(error: any, field: FormlyFieldConfig) {
  if (error?.uppercaseNumberMissing) {
    return `${field.props?.label} should have at least 1 uppercase character and 1 number`;
  } else if (error?.uppercaseMissing) {
    return `${field.props?.label} should have at least 1 uppercase character`;
  } else if (error?.numberMissing) {
    return `${field.props?.label} should have at least 1 number`;
  }

  return null;
}
// Password match

export function fieldMatchValidator(control: AbstractControl) {
  const { password, retypePassword } = control.value;

  // avoid displaying the message error when values are empty
  if (!retypePassword || !password) {
    return null;
  }

  if (retypePassword === password) {
    return null;
  }

  return { fieldMatch: { message: 'Password Not Matching' } };
}
