import { Component } from '@angular/core';
import { FieldArrayType } from '@ngx-formly/core';

@Component({
    selector: 'formly-repeat-section',
    template: `
    <div *ngFor="let field of field.fieldGroup; let i = index;">
      <formly-field [field]="field"></formly-field>
    </div>
    <div style="margin:30px 0;">
      <button class="btn btn-primary" type="button" (click)="add()">
      Add row</button>
    </div>
  `,
})
export class RepeatTypeComponent extends FieldArrayType {
    ngOnInit() {
        console.log('model', this.model);
        this.field.templateOptions!['remove'] = this.remove.bind(this);
    }
}