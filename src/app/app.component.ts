import { Component } from '@angular/core';
import { AbstractControl, FormGroup, ValidationErrors } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { emailValidator, passwordValidator } from './app.module';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  form = new FormGroup({});
  model: any = {};
  options: FormlyFormOptions = {};
  showAdditionalFields = false;

  fields: FormlyFieldConfig[] = [
    {
      fieldGroupClassName: 'row',
      fieldGroup: [
        {
          className: 'col-6',
          type: 'input',
          key: 'firstName',
          props: {
            label: 'First Name',
            required: true,
            minLength: 3
          },
        },
        {
          className: 'col-6',
          type: 'input',
          key: 'lastName',
          props: {
            label: 'Last Name',
            minLength: 2
          },
          // expressions: {
          //   'props.disabled': '!model.firstName',
          // },
        },
      ],
    },
    {
      fieldGroupClassName: 'row',
      fieldGroup: [
        {
          className: 'col-6',
          type: 'input',
          key: 'email',
          props: {
            label: 'Email',
            required: true
          },
          validators: {
            validation: [emailValidator]
          }
        },
        {
          className: 'col-3',
          type: 'input',
          key: 'password',
          templateOptions: {
            type: 'password',
            label: 'Password',
            required: true,
            minLength: 6,
          },
          validators: {
            uppercase: {
              expression: (c: AbstractControl) => !c.value || /[A-Z]/.test(c.value),
              message: 'Password should have at least 1 uppercase letter',
            },
            lowercase: {
              expression: (c: AbstractControl) => !c.value || /[a-z]/.test(c.value),
              message: 'Password should have at least 1 lowercase letter',
            },
            number: {
              expression: (c: AbstractControl) => !c.value || /\d/.test(c.value),
              message: 'Password should have at least 1 number',
            },
          }
        },
        {
          className: 'col-3',
          type: 'input',
          key: 'retypePassword',
          templateOptions: {
            type: 'password',
            label: 'Re-type Password',
            required: true,
            minLength: 6,
          },
          validators: {
            fieldMatch: {
              expression: (control: AbstractControl) => {
                const value = control.value;
                return value === this.model.password;
              },
              message: 'Passwords do not match',
            },
          }
        }],
    },
    {
      fieldGroupClassName: "row",
      fieldGroup: [
        {
          key: 'gender',
          type: 'radio',
          className: 'col-3',
          defaultValue: 'Female',
          templateOptions: {
            label: 'Gender',
            required: true,
            options: [
              {
                value: 'Male',
                label: 'Male'
              },
              {
                value: 'Female',
                label: 'Female'
              }
            ],
          },
        },
        {
          key: 'interest',
          type: 'multicheckbox',
          className: 'col-3',
          templateOptions: {
            label: 'Interest',
            required: true,
            options: [
              {
                value: 'Cricket',
                label: 'Cricket'
              },
              {
                value: 'Gaming',
                label: 'Gaming'
              },
              {
                value: 'Traveling',
                label: 'Traveling'
              },
              {
                value: 'Writing',
                label: 'Writing'
              }
            ]
          },
          expressionProperties: {
            'templateOptions.required': (model: any, formState: any) => {
              return model['gender'] == 'Male'
            }
          }
        },
        {
          className: 'col-4',
          key: 'dob',
          type: 'input',
          templateOptions: {
            label: 'Date of Birth',
            type: 'date',
            required: true
          },
          validators: {
            dob: {
              expression: (c: AbstractControl) => {
                if (!c.value) {
                  return true; // Allow empty value (not required)
                }

                // Check if value is a valid date and user is at least 18 years old
                const dob = new Date(c.value);
                const today = new Date();
                const age = today.getFullYear() - dob.getFullYear();
                const monthDiff = today.getMonth() - dob.getMonth();
                if (monthDiff < 0 || (monthDiff === 0 && today.getDate() < dob.getDate())) {
                  return age - 1 >= 18;
                }
                return age >= 18;
              },
              message: (error: any, field: FormlyFieldConfig) => 'The user must be at least 18 years old',
            },
          },
        }
      ],
    },
    {
      template: '<hr /><div><strong></strong></div>',
    },
    {
      key: "address",
      hide: true,
      wrappers: ["panel"],
      templateOptions: { label: "Address" },
      fieldGroupClassName: "row",
      fieldGroup: [
        {
          key: 'addresses',
          fieldGroupClassName: "row",
          type: 'repeat',
          templateOptions: {
            addText: 'Add row',
          },
          fieldArray: {
            fieldGroup: [
              {
                key: "town",
                type: "input",
                className: "col-4",
                templateOptions: {
                  required: true,
                  type: "text",
                  label: "Town"
                }
              },
              {
                key: "state",
                type: "input",
                className: "col-4",
                templateOptions: {
                  required: true,
                  type: "text",
                  label: "State"
                }
              },
              {
                key: "country",
                type: "input",
                className: "col-4",
                templateOptions: {
                  required: true,
                  type: "text",
                  label: "Country"
                },
              },
              {
                type: 'repeat-delete',
                templateOptions: {
                  label: 'Remove',
                },
              },
            ]
          }
        }
      ]
    },
  ]
  showAddressPanel: boolean = false;

  hideShow(event: any) {
    const field = this.fields.find(f => f.key === 'address');
    field!.hide = !field!.hide;

    if (field!.hide) {
      field!.templateOptions!.label = 'Add Address';
      this.form.get('address.town')?.reset();
      this.form.get('address.state')?.reset();
      this.form.get('address.country')?.reset();
    }

    this.showAddressPanel = !this.showAddressPanel;
  }

  hideShowBtn(event: any) {
    this.showAddressPanel = !this.showAddressPanel;
  }
  submit() {
    this.markAllFieldsAsTouched(this.form);
    if (this.form.valid) {
      const modelCopy = { ...this.model };
      modelCopy.password = '*'.repeat(modelCopy.password.length);
      modelCopy.retypePassword = '*'.repeat(modelCopy.retypePassword.length);
      console.log(modelCopy);
    }

  }

  private markAllFieldsAsTouched(control: AbstractControl) {
    if (control instanceof FormGroup) {
      Object.keys(control.controls).forEach(key => {
        const childControl = control.controls[key];
        this.markAllFieldsAsTouched(childControl);
      });
    } else {
      control.markAsTouched();
    }
  }

  // Password Match validation

  passwordMatch(control: AbstractControl): ValidationErrors | null {
    const password = control.root.get('password');
    const retypePassword = control.value;
    if (password && password.value !== retypePassword) {
      return { passwordMatch: true };
    }
    return null;
  }

  // Age validation
  validateAge(control: AbstractControl): ValidationErrors | null {
    const dob = control.value;
    if (!dob) {
      return null;
    }

    const today = new Date();
    const birthDate = new Date(dob);
    let age = today.getFullYear() - birthDate.getFullYear();
    const monthDiff = today.getMonth() - birthDate.getMonth();
    if (monthDiff < 0 || (monthDiff === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }

    return age >= 18 ? null : { ageInvalid: 'User cannot be less than 18 years old' };
  }

  // Add address field
  toggleAddressFields() {
    console.log('Toggle address field Activated', this.fields)
    const addressField = this.fields.find(field => field.key === 'address');
    if (!this.showAddressPanel) {
      this.showAddressPanel = true;
      addressField!.hide = false;
      this.fields.push({
        fieldGroupClassName: "row",
        fieldGroup: [
          {
            key: "town",
            type: "input",
            className: "col-4",
            templateOptions: {
              required: true,
              type: "text",
              label: "Town"
            }
          },
          {
            key: "state",
            type: "input",
            className: "col-3",
            templateOptions: {
              required: true,
              type: "text",
              label: "State"
            }
          },
          {
            key: "country",
            type: "input",
            className: "col-3",
            templateOptions: {
              required: true,
              type: "text",
              label: "Country"
            }
          },
          {
            className: "col-2",
            template: '<button type="button" class="btn btn-danger" (click)="removeAddressFields()">Remove</button>'
          },
        ]
      });
    }
  }

  removeAddressFields() {
    this.showAddressPanel = false;
    const addressFieldIndex = this.fields.findIndex(field => field.key === 'address');
    if (addressFieldIndex !== -1) {
      this.fields.splice(addressFieldIndex + 1, 1); // Remove the dynamically added fields
      this.model.address = {}; // Reset address fields
    }
  }


}