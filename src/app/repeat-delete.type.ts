import { Component } from "@angular/core";
import { FieldType } from "@ngx-formly/core";

@Component({
    selector: "formly-field-button",
    template: `
    <div>
      <button
        [type]="to.type"
        [ngClass]="'btn btn-' + to['btnType']"
        (click)="onClick($event)"
      >
        {{ to['text'] }}
      </button>
    </div>
    <button class="btn btn-danger" type="button" (click)="remove()">
      Remove
    </button>
  `
})
export class RepeatDeleteTypeComponent extends FieldType {
    remove() {
        const i = this.field.parent!.key;

        this.field.parent!.parent!.templateOptions!['remove'](i);
    }
    onClick($event: Event) {
        if (this.props['onClick']) {
            this.props['onClick']($event);
        }
    }

}
